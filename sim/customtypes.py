from dataclasses import dataclass
from datetime import time
from typing import NamedTuple


class Period(NamedTuple):
    """Тип объекта для временного промежутка"""
    start: time
    end: time


@dataclass
class Statistics:
    """
    Статистика
        meters_passed - пройдено метров
        time_working_sbu - время работы СБУ
        time_working_pdm - врему работы ПДМ
        total_ore_mined - общая масса добытой руды
        ore_residue - остаток оторванной породы
        count_explosions - колличество взрывов
    """
    meters_passed: float
    time_working_sbu: time
    time_working_pdm: time
    total_ore_mined: int
    ore_residue: int
    count_explosions: int

