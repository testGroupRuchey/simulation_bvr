from typing import Iterator
from datetime import datetime, timedelta, time
from models import Place, PDM, SBU
from customtypes import Period
from math import ceil


def time_to_int(t: time) -> int:
    """Преобразование объекта time в минуты"""
    hour = t.hour
    minute = t.minute
    minutes = hour * 60 + minute
    return minutes

def int_to_time(minutes: int) -> time:
    hour = minutes // 60
    minute = minutes % 60
    return time(hour, minute)


class Shift:
    """Рабочая смена"""

    def __init__(self):
        self.shifts = (
            (Period(time(9, 0), time(15, 0))),
            (Period(time(17, 0), time(23, 0))),
            (Period(time(1, 0), time(7, 0))),
        )

    def get_current_shift(self, moment: time) -> int:
        """Вернуть номер рабочей смены"""

        for i, s in enumerate(self.shifts):
            if s.start <= moment <= s.end:
                return i + 1
        return 0

    def is_work_time(self, moment: time) -> bool:
        """Проверка, что точка времени попадает в рабочую смену"""

        return bool(self.get_current_shift(moment))

    def time_to_end_shift(self, moment: time) -> time:
        """Вернуть остаток времени до конца смены"""
        current_shift = self.get_current_shift(moment)
        if current_shift:
            minutes_end = time_to_int(self.shifts[current_shift-1].end)
            minutes_moment = time_to_int(moment)
            remaining_time = int_to_time(minutes_end-minutes_moment)
            return remaining_time
        return time(0)


class Operator:

    def __init__(self):
        self.place_1 = Place(1)
        self.place_2 = Place(2)
        self.place_3 = Place(3)
        self.place_4 = Place(4)
        self.places = (
            self.place_1,
            self.place_2,
            self.place_3,
            self.place_4,
        )

        self.place_1.cycle = 'breakway'
        self.place_1.breakway_length(3.2)
        self.place_2.cycle = 'breakway'
        self.place_3.cycle = 'driling'
        self.place_4.cycle = 'charging'

        self.start_time = datetime(2022, 6, 3, 13, 0)
        self.end_time = datetime(2022, 6, 4, 21, 0)
        self.step_time = timedelta(minutes=1)
        self.shift = Shift()
        self.pdm1 = PDM()
        self.pdm2 = PDM()
        self.sbu = SBU()


    def get_place_by_cycle(self, cycle: str) -> Iterator[Place]:
        """Вернуть выработку по состоянию цикла"""
        for place in self.places:
           if place.cycle == cycle:
             yield place

    def ore_shipment(self, date: datetime, places_breakway: Iterator[Place]):
        """Процесс отгрузки руды"""
        try:
            place_breakway = next(places_breakway)
            remainder_length = place_breakway.remainder_length
            remainder_mass = place_breakway.remainder_mass
            cnt_ladle = ceil(remainder_mass / self.pdm1.capacity)
            takes_time = cnt_ladle * self.pdm1.flight_time
            time_end_shift = self.shift.time_to_end_shift(date.time())
        except:
            pass

    def start(self):
        date = self.start_time
        places_breakway = self.get_place_by_cycle('breakway')
        places_drilling = self.get_place_by_cycle('drilling')
        places_charging = self.get_place_by_cycle('charging')
        places_explosion = self.get_place_by_cycle('explosion')

        while date <= self.end_time:
            self.ore_shipment(date, places_breakway)
            date += self.step_time


if __name__ == '__main__':
    pass

