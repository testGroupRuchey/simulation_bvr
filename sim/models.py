from customtypes import Statistics
from datetime import time


class SBU:
    """самоходная буровая установка - СБУ"""

    def __init__(self):
        """
        charging_time - время заряжения в минутах
        drilling_time - время бурения в минутах
        explosion_time - время взрыва в минутах
        drilling_depth - глубина бурения шпуры в метрах
        breakaway_depth - глубина отрыва породы после взрыва в метрах
        is_free - машина свободна и готова к работе
        """
        self.charging_time = 60
        self.drilling_time = 150
        self.explosion_time = 0
        self.drilling_depth = 3.4
        self.breakaway_depth = 3.2
        self.is_free = True

        self.count_explosions = 0
        self.count_chargings = 0
        self.count_drillings = 0

    def get_operating_time(self):
        """Вернуть общее время работы"""
        return sum([self.get_time_charging, self.get_time_drilling, self.get_time_explosion])

    def get_volume_rock_mass(self, sectional_area, rock_mass_density):
        """
        Вернуть объём отрыва.
            sectional_area - площадь сечения выработки в куб.м
            rock_mass_density - плотность горной массы
        """
        vol = sectional_area * rock_mass_density * self.breakaway_depth
        return vol

    def cycle_chargin(self):
        """Произвести цикл заряжания"""
        self.count_chargings += 1
        return self.count_chargings

    def cycle_drilling(self):
        """Произвести цикл бурения"""
        self.count_drillings += 1
        return self.count_drillings

    def cycle_explosion(self):
        """Произвести цикл взрыва"""
        self.count_explosions += 1
        return self.count_explosions

    def get_time_charging(self):
        opertime = self.charging_time * self.count_chargings
        return opertime

    def get_time_drilling(self):
        opertime = self.drilling_time * self.count_drillings
        return opertime

    def get_time_explosion(self):
        opertime = self.explosion_time * self.count_explosions
        return opertime


class PDM:
    """погрузочно – доставочная машина - ПДМ"""

    def __init__(self):
        """
        capacity - вместимость ковша в тоннах
        flight_time - длительность рейса в минутах
        is_free - машина свободна и готова к работе
        """
        self.capacity = 7
        self.flight_time = 10
        self.is_free = True

        self.count_flight = 0

    def cycle_flight(self) -> int:
        self.count_flight += 1
        return self.count_flight

    def get_time_flight(self) -> int:
        """Вернуть общее время рейсов"""
        opertime = self.count_flight * self.flight_time
        return opertime

    def get_total_mass(self) -> int:
        """Вернуть общее количество добытой породы"""
        total = self.capacity * self.count_flight
        return total

    def get_capacity(self) -> int:
        """Вернуть вместимость ковша"""
        return self.capacity

    def get_flight_time(self) -> int:
        """Вернуть время одного рейса"""
        return self.flight_time

    def reset_indicators(self) -> None:
        """Сбросить показатели рейсов и добытой руды"""
        self.count_flight = 0


class Place:
    """Выработка"""

    def __init__(self, number):
        """
        sectional_area - площадь сечения выработки в кв.м
        rock_mass_density - плотность горной массы в куб.м
        """
        self.number = number
        self.sectional_area = 20
        self.length = 200
        self.rock_mass_density = 3.4

        self.cycles = {
            'drilling': 'Бурение шпуров',
            'charging': 'Заряжание',
            'explosion': 'Взрыв',
            'breakway': 'Откатка'
        }
        self.current_cycle = self.cycles['drilling']
        self.stat = Statistics(
                meters_passed = 0,
                time_working_sbu = time(0),
                time_working_pdm = time(0),
                total_ore_mined = 0,
                ore_residue = 0,
                count_explosions = 0
        )
        self.mass_remainder = 0

    def __str__(self):
        return f'Выработка №{self.number}'

    @property
    def cycle(self) -> str:
        return self.current_cycle

    @cycle.setter
    def cycle(self, cycle_name: str) -> None:
        self.current_cycle = self.cycles[cycle_name]

    @property
    def remainder_length(self) -> float:
        """Вернуть остаток длины выработки"""
        return self.length - self.stat.meters_passed

    @property
    def remainder_mass(self) -> float:
        """Вернуть остаток массы породы после отрыва"""
        return self.mass_remainder

    @remainder_mass.setter
    def remainder_mass(self, m: int) -> None:
        """Установить остаток массы породы после отрыва"""
        self.mass_remainder = m

    def breakway_length(self, length: float) -> None:
        """Создать отрыв породы на длину length в метрах"""
        self.mass_remainder = self.sectional_area * self.rock_mass_density
        self.stat.meters_passed += length

